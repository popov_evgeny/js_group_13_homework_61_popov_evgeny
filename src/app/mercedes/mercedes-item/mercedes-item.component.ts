import { Component, Input, OnInit } from '@angular/core';
import { Mercedes } from '../../shared/mercedes.model';
import { MercedesService } from '../../shared/Mercedes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mercedes-item',
  templateUrl: './mercedes-item.component.html',
  styleUrls: ['./mercedes-item.component.css']
})
export class MercedesItemComponent implements OnInit {
  @Input() car!: Mercedes;
  bmwCatalog!: Mercedes[];

  constructor(
    private mercedesService: MercedesService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.bmwCatalog = this.mercedesService.addMercedesCatalog();
  }

  onClick() {
    void this.router.navigate(['form/']);
  }
}
