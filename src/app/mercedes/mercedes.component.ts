import { Component, OnInit } from '@angular/core';
import { Mercedes } from '../shared/mercedes.model';
import { MercedesService } from '../shared/Mercedes.service';

@Component({
  selector: 'app-mercedes',
  templateUrl: './mercedes.component.html',
  styleUrls: ['./mercedes.component.css']
})
export class MercedesComponent implements OnInit {
  mercedesCatalog!: Mercedes[];

  constructor( private mercedesService: MercedesService ) {}

  ngOnInit(): void {
    this.mercedesCatalog = this.mercedesService.addMercedesCatalog();
  }
}
