import { Component, OnInit } from '@angular/core';
import { BMW } from '../shared/bmw.model';
import { BmwService } from '../shared/bmw.service';

@Component({
  selector: 'app-bmw',
  templateUrl: './bmw.component.html',
  styleUrls: ['./bmw.component.css']
})
export class BmwComponent implements OnInit {
  bmwCatalog!: BMW[];

  constructor( private bmwService: BmwService) {}

  ngOnInit(): void {
    this.bmwCatalog = this.bmwService.addBmwCatalog();
  }
}
