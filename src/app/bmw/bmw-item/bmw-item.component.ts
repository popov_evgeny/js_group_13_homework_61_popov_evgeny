import { Component, Input, OnInit } from '@angular/core';
import { BMW } from '../../shared/bmw.model';
import { BmwService } from '../../shared/bmw.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bmw-item',
  templateUrl: './bmw-item.component.html',
  styleUrls: ['./bmw-item.component.css']
})
export class BmwItemComponent implements OnInit {
  @Input() car!: BMW;
  bmwCatalog!: BMW[];

  constructor(
    private bmwService: BmwService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.bmwCatalog = this.bmwService.addBmwCatalog();
  }

  onClick() {
    void this.router.navigate(['form/']);
  }

}
