import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AudiService } from './shared/audi.service';
import { AudiComponent } from './audi/audi.component';
import { AudiItemComponent } from './audi/audi-item/audi-item.component';
import { BmwItemComponent } from './bmw/bmw-item/bmw-item.component';
import { BmwComponent } from './bmw/bmw.component';
import { BmwService } from './shared/bmw.service';
import { MercedesComponent } from './mercedes/mercedes.component';
import { MercedesItemComponent } from './mercedes/mercedes-item/mercedes-item.component';
import { MercedesService } from './shared/Mercedes.service';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found.component';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'audi/catalog', component: AudiComponent},
  {path: 'bmw/catalog', component: BmwComponent},
  {path: 'mercedes/catalog', component: MercedesComponent},
  {path: 'form', component: FormComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    AudiComponent,
    AudiItemComponent,
    BmwItemComponent,
    BmwComponent,
    MercedesComponent,
    MercedesItemComponent,
    ToolbarComponent,
    FooterComponent,
    HomeComponent,
    NotFoundComponent,
    FormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [AudiService, BmwService, MercedesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
