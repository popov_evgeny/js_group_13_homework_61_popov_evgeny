import { Component, Input, OnInit } from '@angular/core';
import { Audi } from '../../shared/audi.model';
import { AudiService } from '../../shared/audi.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-audi-item',
  templateUrl: './audi-item.component.html',
  styleUrls: ['./audi-item.component.css']
})
export class AudiItemComponent implements OnInit {
  @Input() car!: Audi;
  audiCatalog!: Audi[];

  constructor(
    private audiService: AudiService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.audiCatalog = this.audiService.addAudiCatalog();
  }

  onClick() {
    void this.router.navigate(['form/']);
  }

}
