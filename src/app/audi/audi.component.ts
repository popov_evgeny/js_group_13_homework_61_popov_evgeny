import { Component, OnInit } from '@angular/core';
import { Audi } from '../shared/audi.model';
import { AudiService } from '../shared/audi.service';

@Component({
  selector: 'app-audi',
  templateUrl: './audi.component.html',
  styleUrls: ['./audi.component.css']
})
export class AudiComponent implements OnInit {
  audiCatalog!: Audi[];

  constructor( private audiService: AudiService ) {}

  ngOnInit(): void {
    this.audiCatalog = this.audiService.addAudiCatalog();
  }
}
