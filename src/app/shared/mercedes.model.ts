export class Mercedes {
  constructor(
    public brand: string,
    public name: string,
    public image: string,
    public description: string[],
  ) {}
}
