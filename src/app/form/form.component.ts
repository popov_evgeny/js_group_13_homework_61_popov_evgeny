import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  @ViewChild('inputName') inputName!: ElementRef;
  @ViewChild('inputLastName') inputLastName!: ElementRef;
  @ViewChild('inputEmail') inputEmail!: ElementRef;

  constructor(private router: Router) {}

  onClick() {
    if (this.inputName.nativeElement.value === '' || this.inputLastName.nativeElement.value === '' || this.inputEmail.nativeElement.value === '') {
      alert('Заполните пожфлуйста все поля формы');
    } else {
      alert('Ваша форма отправлена, Спасибо!');
      void this.router.navigate(['/']);
    }
  }
}
