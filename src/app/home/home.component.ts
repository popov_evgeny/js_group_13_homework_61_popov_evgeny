import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  img = 'https://uhdwallpapers.org/uploads/converted/19/01/13/mercedes-benz-cla-250-amg-1600x900_578865-mm-90.jpg'

  constructor() {}

  ngOnInit(): void {
    this.add();
  }


  add() {
    const images = ['https://www.audi-altufievo.ru/content/dam/iph/international/ru/RUS00329/models/rs6_avant/teaser/RSsneak15.jpg', 'https://oboi.ws/filters/gotham_17_3854_oboi_mersedes_1920x1080.jpg', 'https://img1.akspic.ru/originals/6/2/1/8/5/158126-bmw_4_j_serii-bajerishe_motoren_verke_ag-bmv_m4-legkovyye_avtomobili-bmw-1920x1080.jpg']
    let index = 0;

    setInterval(() => {
      if (index !== images.length){
        this.img = images[index];
        index++
      } else {
        index = 0;
      }
    }, 2000)
  }
}
